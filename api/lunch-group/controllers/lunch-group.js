'use strict';

const { sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    async findByOrganization(ctx) {
        let entities = [];
        entities = await strapi.services['lunch-group'].findByOrganization(ctx.params);
        return entities.map(entity => sanitizeEntity(entity, { model: strapi.models['lunch-group'] }));
    },

    async create(ctx) {
        const foo = await strapi.services['lunch-group'].findByUser(ctx.req.user.id)
        const isUserNotInLunchGroup = foo.map(entity => sanitizeEntity(entity, { model: strapi.models['lunch-group'] })).length;
        if (isUserNotInLunchGroup === 0) {
            let entity;
            if (ctx.is('multipart')) {
                const { data, files } = parseMultipartData(ctx);
                entity = await strapi.services['lunch-group'].create(data, { files });
            } else {
                entity = await strapi.services['lunch-group'].create(ctx.request.body);
            }
            return sanitizeEntity(entity, { model: strapi.models['lunch-group'] });
        } else {
            return ctx.badRequest('Il semble que vous soyez déjà dans un groupe, merci de le quitter avant d\'en rejoindre un autre.')
        }
    },

    async update(ctx) {
        const foo = await strapi.services['lunch-group'].findByUser(ctx.req.user.id)
        const isUserNotInLunchGroup = foo.map(entity => sanitizeEntity(entity, { model: strapi.models['lunch-group'] })).length;
        if (isUserNotInLunchGroup === 0) {
            const { id } = ctx.params;
            let entity;
            if (ctx.is('multipart')) {
                const { data, files } = parseMultipartData(ctx);
                entity = await strapi.services['lunch-group'].update({ id }, data, {
                    files,
                });
            } else {
                entity = await strapi.services['lunch-group'].update({ id }, ctx.request.body);
            }
            return sanitizeEntity(entity, { model: strapi.models['lunch-group'] });
        } else {
            return ctx.badRequest('Il semble que vous soyez déjà dans un groupe, merci de le quitter avant d\'en rejoindre un autre.')
        }
    },

    async leave(ctx) {
        const { id } = ctx.params;
        let entity;
        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
            entity = await strapi.services['lunch-group'].update({ id }, data, {
                files,
            });
        } else {
            entity = await strapi.services['lunch-group'].update({ id }, ctx.request.body);
        }
        const sanatizeEntity = sanitizeEntity(entity, { model: strapi.models['lunch-group'] });
        if (sanatizeEntity.users.length === 0) {
            strapi.services['lunch-group'].delete({ id: sanatizeEntity.id });
        }
        return sanatizeEntity;
    }
};
