'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
    /**
     * Promise to fetch all records
     *
     * @return {Promise}
     */
    async findByOrganization(params, populate) {
        const today = new Date();
        const date = today.getFullYear() + '-' + (today.getMonth() > 10 ? (today.getMonth() + 1) : "0" + (today.getMonth() + 1)) + '-' + (today.getDate() > 10 ? today.getDate() : "0" + today.getDate());

        const result = await strapi
            .query('lunch-group')
            .model.query(qb => {
                qb.join('organizations', 'organizations.id', 'lunch_groups.organization');
                qb.where({ 'organizations.id': params.organization_id });
                qb.where({ 'date': date });
            })
            .fetchAll();
        return result;
    },

    /**
     * Promise to fetch all records
     *
     * @return {Promise}
     */
    async findByUser(userID) {
        const today = new Date();
        const date = today.getFullYear() + '-' + (today.getMonth() > 10 ? (today.getMonth() + 1) : "0" + (today.getMonth() + 1)) + '-' + (today.getDate() > 10 ? today.getDate() : "0" + today.getDate());

        const result = await strapi
            .query('lunch-group')
            .model.query(qb => {
                qb.where({ 'date': date });
                qb.join('lunch_groups_users__users_lunch_groups', 'lunch_groups_users__users_lunch_groups.lunch-group_id', 'lunch_groups.id');
                qb.where({ 'user_id': userID });
            })
            .fetchAll();
        return result;
    },
};