'use strict';

const { Client, defaultAxiosInstance } = require("@googlemaps/google-maps-services-js");
const url = require("url");

const MAX_PHOTO_WIDTH = 400;

const mime = require('mime');
const fs = require('fs');
const rootDir = process.cwd();

async function getPlacePhoto(place) {
    const client = new Client({});

    return await client.placePhoto({
        params: {
            photo_reference: place.photos[0].photo_reference,
            maxwidth: MAX_PHOTO_WIDTH,
            key: process.env.MAPS_API_KEY,
        },
        timeout: 10000,
    })
        .then(async (r) => {
            console.log("sucess", r.data);
            const fileName = place.place_id + '.jpg';
            const filePath = `${rootDir}/public/uploads/${fileName}`

            console.log({
                path: filePath,
                name: fileName,
                type: mime.getType(filePath),
            })

            await strapi.plugins.upload.services.upload.upload({
                data: r.data,
                files: {
                    path: filePath,
                    name: fileName,
                    type: mime.getType(filePath),
                },
            });
        })
        .catch((e) => {
            console.log("error", e);
        });
}

function handleResults(results, organizationID) {
    results.forEach(async (result) => {
        // const placePhoto = getPlacePhoto(result);

        const restaurant = {
            coords: { ...result.geometry.location },
            name: result.name,
            id: result.place_id,
        }

        strapi.services.restaurant.create({
            name: restaurant.name,
            Position: {
                latitude: restaurant.coords.lat,
                longitude: restaurant.coords.lng,
            },
            organizations: [
                organizationID
            ],
            place_id: restaurant.id,
            // picture: placePhoto
        })
    });
}

module.exports = {
    lifecycles: {
        afterCreate(result) {
            const organizationID = result.id;
            const coords = result.Place;
            const client = new Client({});

            if (process.env.NODE_ENV === "production" && process.env.QUOTAGUARDSTATIC_URL) {
                const proxy = url.parse(process.env.QUOTAGUARDSTATIC_URL);
                defaultAxiosInstance.defaults.proxy = {
                    host: proxy.hostname,
                    port: proxy.port || 80,
                };
                defaultAxiosInstance.defaults.headers["Proxy-Authorization"] = "Basic " + (new Buffer(proxy.auth).toString("base64"));
            }

            client
                .placesNearby({
                    params: {
                        location: coords.latitude + "," + coords.longitude,
                        radius: '500',
                        type: "restaurant",
                        key: process.env.MAPS_API_KEY,
                    },
                    timeout: 1000,
                }, defaultAxiosInstance)
                .then((r) => {
                    const results = r.data.results
                    handleResults(results, organizationID);
                })
                .catch((e) => {
                    console.log(e.response);
                });
        },
    }
};
